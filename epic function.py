# COPY ALL CODE TO IDLE AND WORK THROUGH THE QUESTIONS AND TODOS IN THE COMMENTS
#Q what data type in python uses curly brackets?
#Q what data type in python uses square brackets?
#Q what do you think the paired values relate to in this example?
student_scores = {
	"jony":[3,3,4],
	"amy":[6,6,6]
}

# functions to process the dictionary
def print_scores():
	#Q how many times will this loop iterate?
	for student in student_scores:
		print (student) # this will print the left hand side of the pair
		print (student_scores[student]) #this will print the right hand side
		# Q what calculation is being performed here?
		print (sum (student_scores[student])/len(student_scores[student]))

def ind_average(chosen):
	for student in student_scores:
		if student in student_scores:
			#Q how many times will this return a value?
			if student == chosen:
				return (sum (student_scores[student])/len(student_scores[student]))
		else:
			return("I can't find that student!")

def ind_scores(chosen):
	for student in student_scores:
		if student == chosen:
			return (student_scores[student])

def print_individual(chosen):
	for student in student_scores:
		if student == chosen:
			print (student)
			print (student_scores[student])
			print (sum (student_scores[student])/len(student_scores[student]))


#Q Which functions are procedures?
#Q Which functions are fruitful?
#Q What is the order of line execution for this program assuming that the user chooses "i" and "amy"


#TODO Run the program and comment all lines of code
#test functions these can be used to check that the functions above work :
def test_dictionary():	
	return "the dictionary created is :",student_scores
	
def test_alloutputs():
	print_scores()

def test_print_individual():
	print_individual("jony")
	print_individual("amy")
	
def test_student(student):
	print(student," scored an average of:")
	print(ind_average(student))
	print ("and her actual scores were: ")
	print(ind_scores(student))

def test_missing():
	print(ind_average("Missing"))

def empty_funct():
	return None
#TODO : test that the dictionary is working syntactically, uncomment the line below and see what it does
#print(test_dictionary())

# Q what is this line below testing?
#test_student("amy")

# TODO 
# Run the testing functions and explain why they are useful when developing the code
